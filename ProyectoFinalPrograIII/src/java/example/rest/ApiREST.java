package example.rest;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.ws.rs.GET;
import javax.ws.rs.core.Response;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xml.sax.SAXException;

@Path("/restapi")
public class ApiREST {

    private static class Date {

        public Date() {
        }
    }

    private class Evento {

        public int codigo;
        public String Nombre;
    }

    @GET
    @Path("/obtenerevento/{EventoSeleccionado}")
    @Produces(MediaType.APPLICATION_JSON)
    /**
     * Lee el archivo de eventos.xml que se encuentra en la carpeta
     * WEB-INF\classes\example\rest Se espera como parametro el codigo de evento
     * para no obtener todos los eventos
     *
     * @return Response
     */
    public Response eventoJsonDesdeXml(@PathParam("EventoSeleccionado") String EventoSeleccionado) throws JSONException, ParserConfigurationException, SAXException, IOException, ParseException {
        /*Se lee el archivo XML de la carpeta mediante un input stream*/
        InputStream leerArchivoXml = ApiREST.class.getResourceAsStream("Eventos.xml");
        /*Con ese resultado ya se obtiene el data input stream que nos servira para crear el resultado json*/
        DataInputStream leerStreamArchivoXml = new DataInputStream(leerArchivoXml);
        StringBuilder stringArchivoXml;
        stringArchivoXml = new StringBuilder();
        String lineaResultadoXml;
        /*Se lee la variable input stream para convertirla en un string que se almacenara en string archivo xml*/
        while ((lineaResultadoXml = leerStreamArchivoXml.readLine()) != null) {
            stringArchivoXml.append(lineaResultadoXml).append("\n");
        }
        /*Se crea el resultado json de la lectura del archivo xml*/
        JSONObject objJsonDelXml = XML.toJSONObject(stringArchivoXml.toString());
        /*Se crea un objeto array que contiene todos los eventos del xml*/
        JSONArray arrayEventos = objJsonDelXml.getJSONObject("EVENTOS").getJSONArray("EVENTO");
        /*Esta variable sera el resultado del filtro*/
        JSONObject objResultado = new JSONObject();
        /*Obtenemos la fecha actual*/
        Calendar calendarioHoy = Calendar.getInstance();
        calendarioHoy.clear(Calendar.HOUR);
        calendarioHoy.clear(Calendar.MINUTE);
        calendarioHoy.clear(Calendar.SECOND);
        java.util.Date Hoy = calendarioHoy.getTime();
        
        for (int n = 0; n < arrayEventos.length(); n++) {
            JSONObject object = arrayEventos.getJSONObject(n);
            
            /*Cuando la fecha no sea mayor a la de hoy se almacenara en el objResultado*/
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            /*Se concatena la fecha del evento y la hora*/
            java.util.Date fechaEvento = format.parse(object.getString("FECHA") + " " + object.getString("HORA"));

            /*Cuando el codigo de parametro coincida con el de la estructura y sea una fecha valida se almacenara en el objResultado*/
            if (object.getString("CATEGORIA").equals(EventoSeleccionado) && Hoy.before(fechaEvento)) {
                objResultado.put("Nombre", object.getString("NOMBRE"));
                objResultado.put("Codigo", object.get("CODIGO"));
                objResultado.put("Fecha", object.get("FECHA"));
                objResultado.put("Hora", object.getString("HORA"));
                objResultado.put("Lugar", object.getString("LUGAR"));
                objResultado.put("Descripcion", object.getString("DESCRIPCION"));
                objResultado.put("Categoria", object.getString("CATEGORIA"));
            }
        }

        /*Se aplica formato y se devuelve en el resultado*/
        String jsonConFormato = objResultado.toString(4);
        return Response.ok(
                jsonConFormato,
                MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/obtenerlocalidades/{codigoevento}")
    @Produces(MediaType.APPLICATION_JSON)
    /**
     * Lee el archivo de eventos.xml que se encuentra en la carpeta
     * WEB-INF\classes\example\rest Se espera como parametro el codigo de evento
     * para no obtener todos los eventos
     *
     * @return Response
     */
    public Response localidadesJsonDesdeXml(@PathParam("codigoevento") String EventoSeleccionado) throws JSONException, ParserConfigurationException, SAXException, IOException {
        /*Se lee el archivo XML de la carpeta mediante un input stream*/
        InputStream leerArchivoXml = ApiREST.class.getResourceAsStream("Eventos.xml");
        /*Con ese resultado ya se obtiene el data input stream que nos servira para crear el resultado json*/
        DataInputStream leerStreamArchivoXml = new DataInputStream(leerArchivoXml);
        StringBuilder stringArchivoXml;
        stringArchivoXml = new StringBuilder();
        String lineaResultadoXml;
        /*Se lee la variable input stream para convertirla en un string que se almacenara en string archivo xml*/
        while ((lineaResultadoXml = leerStreamArchivoXml.readLine()) != null) {
            stringArchivoXml.append(lineaResultadoXml).append("\n");
        }
        /*Se crea el resultado json de la lectura del archivo xml*/
        JSONObject objJsonDelXml = XML.toJSONObject(stringArchivoXml.toString());
        /*Se crea un objeto array que contiene todos los eventos del xml*/
        JSONArray arrayEventos = objJsonDelXml.getJSONObject("EVENTOS").getJSONArray("EVENTO");
        /*Esta variable sera el resultado del filtro*/
        JSONObject objResultado = new JSONObject();

        for (int n = 0; n < arrayEventos.length(); n++) {
            JSONObject object = arrayEventos.getJSONObject(n);
            /*Cuando el codigo de parametro coincida con el de la estructura se almacenara en el objResultado*/
            if (object.getString("CATEGORIA").equals(EventoSeleccionado)) {
                /*Cuando se obtiene el codigo solicitado, se itera hacia las localidades y se obtienen las localidades para ese evento*/
                JSONArray arrayLocalidades = object.getJSONObject("LOCALIDADES").getJSONArray("LOCALIDAD");
                /*Por cada localidad se agrega en el objeto json resultado*/
                for (int j = 0; j < arrayLocalidades.length(); j++) {
                    objResultado.put("Localidades", arrayLocalidades);
                }
            }
        }

        /*Se aplica formato y se devuelve en el resultado*/
        String jsonConFormato = objResultado.toString(4);
        return Response.ok(
                jsonConFormato,
                MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/obtenereventosdisponibles")
    @Produces(MediaType.APPLICATION_JSON)
    /**
     * Lee el archivo de eventos.xml que se encuentra en la carpeta
     * WEB-INF\classes\example\rest Se espera como parametro el codigo de evento
     * para no obtener todos los eventos
     *
     * @return Response
     */
    public Response eventosDisponiblesJsonDesdeXml() throws JSONException, ParserConfigurationException, SAXException, IOException, ParseException {
        /*Se lee el archivo XML de la carpeta mediante un input stream*/
        InputStream leerArchivoXml = ApiREST.class.getResourceAsStream("Eventos.xml");
        /*Con ese resultado ya se obtiene el data input stream que nos servira para crear el resultado json*/
        DataInputStream leerStreamArchivoXml = new DataInputStream(leerArchivoXml);
        StringBuilder stringArchivoXml;
        stringArchivoXml = new StringBuilder();
        String lineaResultadoXml;
        /*Se lee la variable input stream para convertirla en un string que se almacenara en string archivo xml*/
        while ((lineaResultadoXml = leerStreamArchivoXml.readLine()) != null) {
            stringArchivoXml.append(lineaResultadoXml).append("\n");
        }
        /*Se crea el resultado json de la lectura del archivo xml*/
        JSONObject objJsonDelXml = XML.toJSONObject(stringArchivoXml.toString());
        /*Se crea un objeto array que contiene todos los eventos del xml*/
        JSONArray arrayEventos = objJsonDelXml.getJSONObject("EVENTOS").getJSONArray("EVENTO");
        /*Esta variable sera el resultado del filtro*/
        JSONArray objResultado = new JSONArray();
        /*Obtenemos la fecha actual*/
        Calendar calendarioHoy = Calendar.getInstance();
        calendarioHoy.clear(Calendar.HOUR);
        calendarioHoy.clear(Calendar.MINUTE);
        calendarioHoy.clear(Calendar.SECOND);
        java.util.Date Hoy = calendarioHoy.getTime();

        for (int n = 0; n < arrayEventos.length(); n++) {
            JSONObject object = arrayEventos.getJSONObject(n);
            /*Cuando la fecha no sea mayor a la de hoy se almacenara en el objResultado*/
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            /*Se concatena la fecha del evento y la hora*/
            java.util.Date fechaEvento = format.parse(object.getString("FECHA") + " " + object.getString("HORA"));
            /*Validadmos la fecha*/
            if (Hoy.before(fechaEvento)) {
                /*Se agrega al resultado*/
                objResultado.put(object);
            }
        }

        /*Se aplica formato y se devuelve en el resultado*/
        String jsonConFormato = objResultado.toString(4);
        return Response.ok(
                jsonConFormato,
                MediaType.APPLICATION_JSON).build();

    }

}