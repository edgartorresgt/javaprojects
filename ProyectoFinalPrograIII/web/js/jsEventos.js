function obtenerEventoDisponibles() {
    $.ajax({
        url: "http://localhost:8080/ProyectoFinalPrograIII/srvRest/restapi/obtenereventosdisponibles",
        type: 'GET',
        timeout: 10000,
        crossDomain: true,
        dataType: 'json',
        data: {
            format: 'json'
        },
        success: function(data) {
            console.log('obtenereventosdisponibles Correcto');
            for (let x of data) {
                var fila = "<tr>"+
                    "<td title='"+ x.DESCRIPCION +"'>" + x.CODIGO + "</td>" +
                    "<td>" + x.NOMBRE + "</td>" +
                    "<td>" + x.FECHA + " " + x.HORA + "</td>" +
                    "<td>" + x.LUGAR + "</td>" +
                    "<td>" + x.CATEGORIA + "</td>" +
                    "</tr>";
                $("#tblEventosDisponibles").append(fila);
            }
        },
        error: function(e) {
            console.log('Error en la llamada Web API');
            console.log(e.toString());
        }
    });
}

function obtenerEvento(evento) {
    $.ajax({
        url: "http://localhost:8080/ProyectoFinalPrograIII/srvRest/restapi/obtenerevento/" + evento,
        type: 'GET',
        timeout: 10000,
        crossDomain: true,
        dataType: 'json',
        data: {
            format: 'json'
        },
        success: function(data) {
            console.log('obtenerEvento Correcto');
            /*Si el objeto data regresa nulo, es decir si se pudo hacer la consulta pero no trae ningun valor se sale de la funcion*/
            if (!$.trim(data)) {
                return;
            }
            /*Como no es un array, se asignan directamente del objeto data a las filas de la tabla*/
            var fila = "<tr>\n\
                    <td>" + data.Codigo + "</td>" +
                "<td>" + data.Nombre + "</td>" +
                "<td>" + data.Fecha + " " + data.Hora + "</td>" +
                "<td>" + data.Lugar + "</td>" +
                "<td>" + data.Categoria + "</td>" +
                "</tr>";
            /*Cada evento tiene una tabla y una descripcion, por el parametro evento, se agrega la linea y se asigna el valor a la descripcion*/
            switch (evento) {
                case "Concierto":
                    $("#tblEventoConcierto").append(fila);
                    document.getElementById('lblDescEventoConcierto').innerHTML = data.Descripcion + " " + data.Nombre;
                    document.getElementById("localidadConcierto").innerHTML = "Localidad para: " + data.Nombre;
                    var ul = document.getElementById("lstLocalidadConcierto")
                    obtenerLocalidadEvento(evento, ul);
                    break;
                case "Familiares":
                    $("#tblEventoFamiliar").append(fila);
                    document.getElementById('lblDescEventoFamiliar').innerHTML = data.Descripcion;
                    document.getElementById("localidadFamiliar").innerHTML = "Localidad para: " + data.Nombre;
                    var ul = document.getElementById("lstLocalidadFamiliar")
                    obtenerLocalidadEvento(evento, ul);
                    break;
                case "Teatro y Culturales":
                    $("#tblEventoTeatro").append(fila);
                    document.getElementById('lblDescEventoTeatro').innerHTML = data.Descripcion;
                    document.getElementById("localidadTeatro").innerHTML = "Localidad para: " + data.Nombre;
                    var ul = document.getElementById("lstLocalidadTeatro")
                    obtenerLocalidadEvento(evento, ul);
                    break;
                case "Deportes":
                    $("#tblEventoDeporte").append(fila);
                    document.getElementById('lblDescEventoDeporte').innerHTML = data.Descripcion;
                    document.getElementById("localidadDeporte").innerHTML = "Localidad para: " + data.Nombre;
                    var ul = document.getElementById("lstLocalidadDeporte")
                    obtenerLocalidadEvento(evento, ul);

                    break;
                case "Especiales":
                    $("#tblEventoEspecial").append(fila);
                    document.getElementById('lblDescEventoEspecial').innerHTML = data.Descripcion;
                    document.getElementById("localidadEspecial").innerHTML = "Localidad para: " + data.Nombre;
                    var ul = document.getElementById("lstLocalidadEspecial")
                    obtenerLocalidadEvento(evento, ul);
                    break;
            }

        },
        error: function(e) {
            console.log('Error en la llamada Web API');
            console.log(e.toString());
        }
    });
}

function obtenerLocalidadEvento(evento, ul) {
    $.ajax({
        url: "http://localhost:8080/ProyectoFinalPrograIII/srvRest/restapi/obtenerlocalidades/" + evento,
        type: 'GET',
        timeout: 10000,
        crossDomain: true,
        dataType: 'json',
        data: {
            format: 'json'
        },
        success: function(data) {
            console.log('obtener localidades por evento Correcto');
            for (let x of data.Localidades) {
                var li = document.createElement("li");
                li.appendChild(document.createTextNode(x.NOMBRE + " Precio: " + x.PRECIO));
                li.setAttribute("id", x.NOMBRE + " Precio: " + x.PRECIO); //Agregar atributo id, sirve para determinar el elemento seleccionado
                li.setAttribute("class", "elementoseleccionado") //Clase de referencia para el elemento seleccionado
                ul.appendChild(li);
            }
        },
        error: function(e) {
            console.log('Error en la llamada Web API');
            console.log(e.toString());
        }
    });
}



//Genera un objeto Blob con los datos en un archivo TXT
function generarTexto(evento, Localidad) {
    var texto = [];
    texto.push('Datos Compra: ');
    texto.push(' ');
    texto.push(evento);
    texto.push(' ');
    texto.push(Localidad);
    texto.push(' ');
    texto.push('Fecha: ');
    texto.push((new Date()).toLocaleDateString());
    texto.push(' ');
    //El constructor de Blob requiere un Array en el primer 
    //parámetro así que no es necesario usar toString. El 
    //segundo parámetro es el tipo MIME del archivo
    return new Blob(texto, {
        type: 'text/plain'
    });
};

function descargarArchivo(contenidoEnBlob, nombreArchivo) {
    //creamos un FileReader para leer el Blob
    var reader = new FileReader();
    //Definimos la función que manejará el archivo
    //una vez haya terminado de leerlo
    reader.onload = function(event) {
        //Usaremos un link para iniciar la descarga 
        var save = document.createElement('a');
        save.href = event.target.result;
        save.target = '_blank';
        //Truco: así le damos el nombre al archivo 
        save.download = nombreArchivo || 'archivo.dat';
        var clicEvent = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        });
        //Simulamos un clic del usuario
        //no es necesario agregar el link al DOM.
        save.dispatchEvent(clicEvent);
        //Y liberamos recursos...
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    };
    //Leemos el blob y esperamos a que dispare el evento "load";
    reader.readAsDataURL(contenidoEnBlob);
};

/*Ordenar eventos por fecha y nombre de los eventos disponibles*/
/*Inicio*/
/*El parametro numeroEncabezado se envia en el header de la tabla que se va a ordenar, esta funcion es solo para eventos disponibles*/
function OrdenarTablaEventos(numeroEncabezado) {
    var tablaAOrdenar, fila, ordenarFila, i, x, y, debeOrdenarFila, tipoOrden, contador = 0;
    tablaAOrdenar = document.getElementById("tblEventosDisponibles"); //Nombre de la tabla a ordenar
    ordenarFila = true;
    //Se asigna el tipo de orden, puede ser ascendente o descendente, en este caso usaremos ascendente
    tipoOrden = "asc";
    /*Este ciclo se reaizara hasta que cambio de filas sea verdadero, significa que ya fueron ordenadas las filas*/
    while (ordenarFila) {
        //Inicia el ordenamiento, asignando el valor de falso a la variable ordenar fila
        ordenarFila = false;
        fila = tablaAOrdenar.getElementsByTagName("TR");
        /*Se recorren todas las filas, excepto la primera porque contiene los encabezados de la tabla*/
        for (i = 1; i < (fila.length - 1); i++) {
            //inicia asignando esta valor a falso, que no se debe ordenar:
            debeOrdenarFila = false;
            /*Obtiene los elementos que se quieren comparar,
            uno es de la fila actual y el otro el de la siguiente*/
            x = fila[i].getElementsByTagName("TD")[numeroEncabezado];
            y = fila[i + 1].getElementsByTagName("TD")[numeroEncabezado];
            /* verifica si las dos filas deberian ser ordenadas basadas en el orden, si es ascendente o descendente*/
            if (tipoOrden == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //Si es asi, se asigna a verdadero y se sale del loop
                    debeOrdenarFila = true;
                    break;
                }
            } else if (tipoOrden == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //Si es asi, se asigna a verdadero y se sale del loop
                    debeOrdenarFila = true;
                    break;
                }
            }
        }
        if (debeOrdenarFila) {
            /*Si se debe ordenar fila, entonces se realiza el cambio en la tabla y se termina el while*/
            fila[i].parentNode.insertBefore(fila[i + 1], fila[i]);
            ordenarFila = true; 
            //Cada vez que se hace un ordenamiento, se incrementa el contador
            contador++;
        } else {
            /*Si no se ha realizado ningun cambio y la direccion es ascendente, se cambia la direccion a descendente y se inicia el loop de nuevo*/
            if (contador == 0 && tipoOrden == "asc") {
                tipoOrden = "desc";
                ordenarFila = true;
            }
        }
    }
}
/*Fin*/