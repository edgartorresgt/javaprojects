package AnalizadorLexico;

import java.io.*;
import java.util.*;

public class AnalizadorLexico {

    private char caracterLeido = ' ';
    private char caracterLeidoActual = ' ';
    private File ArchivoProcesar = null;
    private Hashtable<String, Palabra> Palabra = new Hashtable<String, Palabra>();
    private ArrayList<Token> listaDeTokens = new ArrayList<>();
    
    public AnalizadorLexico(String rutaArchivo) {
        this.ArchivoProcesar = new File(rutaArchivo);
    }

    private void reserve(Palabra t) {
        Palabra.put(t.lexema, t);
    }

    public void BuscarTokens() throws FileNotFoundException {
        ArrayList<Token> listaDeTokens = new ArrayList<>();
        Scanner sc = new Scanner(ArchivoProcesar);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            for (int i = 0; i < line.length(); i++) {
                caracterLeido = line.charAt(i);
                //Manejo de Comentarios
                //Ejercicio 2.6.1: Extienda el analizador léxico de la sección 2.6.5 para manejo de los comentarios,
                if (caracterLeido == '/') {
                    for (int j = 0; j < line.substring(i).length() - 1; j++) {
                        //Comentario Simple
                        caracterLeidoActual = line.charAt(j);
                        if (caracterLeido == '/' && caracterLeidoActual == '/') {
                            listaDeTokens.add(new Token(caracterLeido));
                            listaDeTokens.add(new Token(caracterLeidoActual));
                            //Bloque Comentario
                        } else if (caracterLeidoActual == '*') {
                            listaDeTokens.add(new Token(caracterLeido));
                            listaDeTokens.add(new Token(caracterLeidoActual));
                            //Busca palabras dentro del comentario 
                            String nuevaPalabra = "";
                            String nuevoString = line.substring(j + 1);
                            // nuevoString es el substring a partir del inicio del bloque de comentarios
                            for (int h = 0; h < line.substring(j + 1).length(); h++) {
                                caracterLeidoActual = nuevoString.charAt(h);
                                if (Character.isAlphabetic(caracterLeidoActual) || Character.isDigit(caracterLeidoActual)) {
                                    nuevaPalabra = nuevaPalabra + String.valueOf(caracterLeidoActual);
                                } else if (caracterLeidoActual == ' ' || !nuevaPalabra.isEmpty()) {
                                    if (!nuevaPalabra.isEmpty()) {
                                        Palabra w = Palabra.get(nuevaPalabra);
                                        w = new Palabra(Etiqueta.ID, nuevaPalabra);
                                        reserve(w);
                                        nuevaPalabra = "";
                                        if (caracterLeidoActual == '*') {
                                            h = h + -1;
                                        }
                                    }
                                    //Busca el final del bloque de comentario
                                } else if (caracterLeidoActual == '*') {
                                    char finBloqueComentario = line.charAt(line.length() - 1);
                                    listaDeTokens.add(new Token(caracterLeidoActual));
                                    listaDeTokens.add(new Token(finBloqueComentario));
                                }
                            }
                            //Avanza a la siguiente linea
                            line = sc.nextLine();
                            //Regresa el caracter al primero de la siguiente linea
                            caracterLeido = line.charAt(0);
                            break;
                        }
                    }
                }
                /*Manejar operadores*/
                //Ejercicio 2.6.2: Extienda el analizador léxico en la sección 2.6.5 para que reconozca los operadores relacionales <,<=, ==, !=, >=, >
                if ("<=!>".indexOf(caracterLeido) > -1) {
                    if (caracterLeido == '<' || caracterLeido == '=' || caracterLeido == '!' || caracterLeido == '>') {
                        /*Operadores compuestos <=, ==, !=, >= */
                        if (line.substring(i).length() > 1) {
                            for (int j = 1; j < line.substring(i).length(); j++) {
                                caracterLeidoActual = line.substring(i).charAt(j);
                                listaDeTokens.add(new Token(caracterLeido));
                                listaDeTokens.add(new Token(caracterLeidoActual));
                                break;
                            }
                        } /*Operadores simples <,>*/ else {
                            listaDeTokens.add(new Token(caracterLeido));
                        }
                    }
                }
            }
        }
        /*Mostrar Tokens*/
        for (int i = 0; i < listaDeTokens.size(); i++) {
            System.out.println("Token Reconocidos " + (char) listaDeTokens.get(i).tag);
        }
        /*Mostar Palabras*/
        Iterator<Map.Entry<String, Palabra>> iterador = Palabra.entrySet().iterator();
        while (iterador.hasNext()) {
            Map.Entry<String, Palabra> entry = iterador.next();
            System.out.println("Palabras Reconocidas " + entry.getValue().lexema);
        }
    }
}