package AnalizadorLexico;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class AnalizadorLex {

    private int linea = 0;
    private char vistazo = ' ';
    private int aux = 0;
    private String rutaArchivo = "";
    public String LineaCodigo = "";
    public Boolean LeerArchivo = false;
    private final Hashtable<String, Palabra> Palabra = new Hashtable<>();
    private final ArrayList<Token> listaDeTokens = new ArrayList<>();
    private final ArrayList<Rel> listaDeOperadores = new ArrayList<>();
    private final ArrayList<Num> listaDeNumeros = new ArrayList<>();
    private List<String> lineasParaAnalizar = new ArrayList<>();

    public AnalizadorLex(String rutaArchivo) throws FileNotFoundException {
        this.rutaArchivo = rutaArchivo;
    }

    private int PosicionSiguienteCaracter(int PosicionActual, int longitudString) {
        int PosicionSiguiente = PosicionActual + 1;
        return PosicionSiguiente < longitudString ? PosicionSiguiente : PosicionActual;
    }

    public void explorar() throws IOException {

        if (LeerArchivo) {
            lineasParaAnalizar = Files.readAllLines(Paths.get(rutaArchivo), StandardCharsets.UTF_8);
        } else {
            lineasParaAnalizar = Arrays.asList(LineaCodigo.split(" "));
        }

        Iterator<String> iterator = lineasParaAnalizar.iterator();
        while (iterator.hasNext()) {
            linea++;
            String lineaActual = iterator.next();
            for (int i = 0; i < lineaActual.length(); i++) {
                vistazo = lineaActual.charAt(i);
                // Manejo de comentarios
                if (vistazo == '/' || vistazo == '*') {
                    //Se agrega Token a la lista 
                    listaDeTokens.add(new Token(vistazo));
                    //Validacion para el caracter actual
                    aux = PosicionSiguienteCaracter(i, lineaActual.length());
                    i = i == aux ? i : aux;
                    vistazo = (char) lineaActual.charAt(i);
                    if (vistazo == '/') {
                        //Se agrega Token a la lista 
                        listaDeTokens.add(new Token(vistazo));
                        // Comentario de linea simple
                        aux = PosicionSiguienteCaracter(i, lineaActual.length());
                        i = i == aux ? i : aux;
                        if (i == lineaActual.length()) {
                            break;
                        }
//Lo comente, busca el siguiente caracter y espera en un loop infinito hasta que sea el fin de linea
//for (;; vistazo = (char)lineaActual.charAt(i)) {
//  if (vistazo == '\n') {
//      break;
//  }
//}
                    } else if (vistazo == '*') {
                        //Se agrega Token a la lista 
                        listaDeTokens.add(new Token(vistazo));

                        // Comentario en bloque
                        char preVistazo = ' ';
                        //Esta variable tendra el valor del caracter actual
                        preVistazo = vistazo;
                        aux = PosicionSiguienteCaracter(i, lineaActual.length());
                        i = i == aux ? i : aux;
                        vistazo = (char) lineaActual.charAt(i);
                        if (preVistazo == '*' && vistazo == '/') {
                            //Se agrega Token a la lista 
                            listaDeTokens.add(new Token(vistazo));
                            break;
                        }
                    }
                }//Fin de buscar comentarios

//Se omite la asignacion de variables en este loop infinito y se lee la secuencia
//for (;; preVistazo = vistazo, vistazo = (char) System.in.read()) {
//  if (preVistazo == '*' && vistazo == '/') {
//      break;
//  }
//}
//Como se leeran mas lineas, esto tampoco es necesario, de lo contrario resultara una excepcion en una nueva linea
// else {
//      throw new IOException("Error en comentarios");
//}
                // Manejo de Signos de relacion
                if ("<=!>".indexOf(vistazo) > -1) {
                    StringBuffer b = new StringBuffer();
                    b.append(vistazo);
                    aux = PosicionSiguienteCaracter(i, lineaActual.length());
                    i = i == aux ? i : aux;
                    vistazo = (char) lineaActual.charAt(i);
                    //vistazo = (char) System.in.read();
                    if (vistazo == '=') {
                        b.append(vistazo);
                    }
                    listaDeOperadores.add(new Rel(b.toString()));
                }//Fin manejo de signos

                //Si inicia con dígito o punto
                if (Character.isDigit(vistazo) || vistazo == '.') {
                    Boolean existePunto = false;
                    StringBuffer b = new StringBuffer();
                    int x = 0;
                    do {
                        x++;
                        if (vistazo == '.') {
                            existePunto = true;
                        }
                        b.append(vistazo);
                        aux = PosicionSiguienteCaracter(i, lineaActual.length());
                        i = i == aux ? i : aux;
                        vistazo = (char) lineaActual.charAt(i);

                    } while (x < lineaActual.length());

//Esta condicion en el while no funciono cuando se hace el loop
//                    } while (existePunto == true
//                            ? Character.isDigit(vistazo)
//                            : Character.isDigit(vistazo) || vistazo == '.');
                    listaDeNumeros.add(new Num(new Float(b.toString())));
                }//Fin manejo de digitos o puntos

                //Palabras   
                if (Character.isLetter(vistazo)) {
                    StringBuffer b = new StringBuffer();
                    int x = 0;
                    do {
                        x++;
                        b.append(vistazo);
                        aux = PosicionSiguienteCaracter(i, lineaActual.length());
                        i = i == aux ? i : aux;
                        vistazo = (char) lineaActual.charAt(i);
                    } while (x < lineaActual.length());
                    //} while (Character.isLetterOrDigit(vistazo));
                    String s = b.toString();
                    Palabra w = Palabra.get(s);
//Antes como era una funcion entonces retornaba valores ahora es un void que devuelve multiples objetos                    
// if (w != null) {
//  //       w;
// }
                    w = new Palabra(Etiqueta.ID, s);
                    Palabra.put(s, w);
//                  return       w;
                }//Fin Busqueda palabras
            }//Fin For para cada linea
        }//Fin While de la lista

        System.out.println("Total Lineas Exploradas: " + linea);
        /*Mostrar Tokens*/
        System.out.println("Token Reconocidos");
        for (int i = 0; i < listaDeTokens.size(); i++) {
            System.out.println("Token Reconocido " + (char) listaDeTokens.get(i).tag);
        }

        /*Mostrar Operadores*/
        System.out.println("Operadores Reconocidos");
        for (int i = 0; i < listaDeOperadores.size(); i++) {
            System.out.println("Operador Reconocido " + listaDeOperadores.get(i).lexeme);
        }

        /*Mostrar Numeros*/
        System.out.println("Numeros Reconocidos");
        for (int i = 0; i < listaDeNumeros.size(); i++) {
            String tipo = listaDeNumeros.get(i).value == Math.round(listaDeNumeros.get(i).value) ? "Entero" : "Decimal";
            System.out.println("Numero Reconocido " + String.valueOf(listaDeNumeros.get(i).value) + " " + tipo);
        }

        /*Mostar Palabras*/
        System.out.println("Palabras Reconocidos");
        Iterator<Map.Entry<String, Palabra>> iterador = Palabra.entrySet().iterator();
        while (iterador.hasNext()) {
            Map.Entry<String, Palabra> entry = iterador.next();
            System.out.println("Palabras Reconocidas " + entry.getValue().lexema);
        }
    }//Fin Void Explorar
}
