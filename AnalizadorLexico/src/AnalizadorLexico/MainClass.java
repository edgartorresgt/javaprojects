package AnalizadorLexico;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author
 */
public class MainClass {

    static Scanner scanner = new Scanner(System.in); //Sirve para recoger texto por consola
    static int select = -1; //opción elegida del usuario

    public static void main(String args[]) throws FileNotFoundException, IOException, SyntaxException {
        //Mientras la opción elegida sea 0,
        
        while (select != 0) {
            //Try catch para evitar que el programa termine si hay un error
            try {
                System.out.println("Elige opción:\n1.- Explorar desde Archivo"
                        + "\n2.- Ingresar valores a explorar\n"
                        + "3.- Usar valores determinados para explorar\n"
                        + "0.- Salir");
                //Recoger una variable por consola
                select = Integer.parseInt(scanner.nextLine());
                String rutaArchivo = "c:\\test\\test.java";
                AnalizadorLex Analizador = new AnalizadorLex(rutaArchivo);

                //Seleccionar Variables
                switch (select) {
                    case 1:
                        Analizador.LeerArchivo = true;
                        Analizador.explorar();
                        break;
                    case 2:
                        Analizador.LeerArchivo = false;
                        System.out.println("Introduzca cadena a explorar:");
                        String cadena = scanner.nextLine();

                        //Mostrar un salto de línea en Java
                        System.out.println("\n");

                        if ("".equals(cadena)) {
                            System.out.println("No puede estar vacio");
                            break;
                        } else {

                            Analizador.LeerArchivo = false;
                            Analizador.LineaCodigo = cadena;
                            Analizador.explorar();
                        }

                        break;
                    case 3:
                        Analizador.LeerArchivo = false;
                        Analizador.LineaCodigo = "// /* */ 1 11.10 Hola Mundo";
                        Analizador.explorar();
                        break;
                    case 0:
                        System.out.println("Adios!");
                        break;
                    default:
                        System.out.println("Opcion no reconocida");
                        break;
                }
                System.out.println("\n"); //Mostrar un salto de línea en Java
            } catch (Exception e) {
                System.out.println("Ocurrio un Error! " + e.getMessage());
            }
        }
    }
}