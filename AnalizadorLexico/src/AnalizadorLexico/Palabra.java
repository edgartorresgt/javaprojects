package AnalizadorLexico;

public class Palabra extends Token{
	public final String lexema;
	public Palabra(int t, String s){
		super(t);
		lexema = new String(s);
	}
}
