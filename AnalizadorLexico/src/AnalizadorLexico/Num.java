package AnalizadorLexico;

public class Num extends Token {
	public final float value;
	public Num(float v){
		super(Etiqueta.NUM);
 		value = v;
	}
}
