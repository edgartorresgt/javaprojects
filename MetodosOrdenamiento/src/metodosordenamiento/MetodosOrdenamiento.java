/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodosordenamiento;

import java.util.Arrays;

/**
 *
 * @author 30203644
 */
public class MetodosOrdenamiento {

    
    public static DatosAordenar datos; 
    static long start=0;
    static long duration=0;
     
     
    public static void main(String[] args) {
        /*Metodo Burbuja*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        burbuja(datos.lista);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo Burbuja: " + duration + " milliseconds.");
        start=0;duration=0;
        /*Metodo Insercion*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        insercion(datos.lista);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo Insercion: " + duration + " milliseconds.");
        start=0;duration=0;
        /*Metodo Seleccion*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        seleccion (datos.lista);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo Seleccion: " + duration + " milliseconds.");
        start=0;duration=0;
        /*Metodo Shell*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        shell (datos.lista);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo Shell: " + duration + " milliseconds.");
        start=0;duration=0;
        /*Metodo QuickSort*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        quicksort (datos.lista,0,datos.lista.length-1);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo Quicksort: " + duration + " milliseconds.");
        start=0;duration=0;
          /*Metodo quicksort_Middle*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        quicksort_Middle (datos.lista,0,datos.lista.length-1);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo quicksort_Middle: " + duration + " milliseconds.");
        start=0;duration=0;
          /*Metodo quicksort_Last*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        quicksort_Last (datos.lista,0,datos.lista.length-1);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo quicksort_Last: " + duration + " milliseconds.");
        start=0;duration=0;        
        /*Metodo mergesort*/
        start = System.currentTimeMillis();
        datos=new DatosAordenar();
        mergesort (datos.lista,0,datos.lista.length-1);
        duration = System.currentTimeMillis() - start;
        System.out.println("Tiempo Metodo mergesort: " + duration + " milliseconds.");
        start=0;duration=0;   
        
        
    }
    
    public static void burbuja(int[] A) {
        int aux;
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = 0; j < A.length - i - 1; j++) {
                if (A[j + 1] < A[j]) { //Compara si el siguiente elemento es menor al actual
//Si es min los intercambia
                    aux = A[j + 1];
                    A[j + 1] = A[j];
                    A[j] = aux;
                }
            }
        }
    }


    public static void insercion(int A[]) {
        int i, j, aux;
        for (i = 1; i < A.length; i++) { // comenzamos desde el segundo elemento hasta el final 
            aux  = A[i]; //guardamos el elemento
            j = i - 1; //y empezamos a comprobar con el anterior
            while ((j >= 0) && (aux < A[j])) { // mientras queden posiciones
// y el valor de aux sea menor que los de la izquierda
                A[j + 1] = A[j]; // se desplaza a la derecha
                j--;
            }
            A[j + 1] = aux; // colocamos aux en su sitio
        }
       //System.out.println(Arrays.toString(A));
    }    
    

    public static void seleccion(int A[]) {
        int i, j, min, pos, aux;
        for (i = 0; i < A.length - 1; i++) { 
// Desde el primer elemento hasta el final del arreglo 
// En un principio el primer elemento es el menor
                    min = A[i]; //Lo asignamos a la variable min
            pos = i; //guardamos su posición
// Buscamos en el resto del array algún elemento menor que la variable min
            for (j = i + 1; j < A.length; j++) {
                if (A[j] < min) { //Compara si es elemento es menor que la variable min
                    min = A[j]; //Si es menor reasigna ese elemento a la variable min
                    pos = j; //guardamos su posición
                }
            }
            if (pos != i) { // si hay alguno menor se intercambia
                aux = A[i];
                A[i] = A[pos];
                A[pos] = aux;
            }
        }
    }


    public static void shell(int[] A) {
        int salto, i, j, k, aux;
        salto = A.length / 2; //En un principio salto será el tamaño del array dividido 2
        while (salto > 0) { //Mientras hayan saltos
            for (i = salto; i < A.length; i++) { //Comenzamos desde el primer salto
                j = i - salto; //j tomara el valor de los saltos
                while (j >= 0) {
                    k = j + salto; //k tomara el valor del siguiente salto
                    if (A[j] <= A[k]) {
                        j = -1; //Para romper el ciclo;
                    } else { //SI A[k] es menor los intercambia con A[j]
                        aux = A[j];
                        A[j] = A[k];
                        A[k] = aux;
                        j -= salto; //j = j - salto;
                    }
                }
            }
            salto = salto / 2; //Recalcula el salto
        }
    }    



    public static void quicksort(int A[], int izq, int der) {
        int pivote = A[izq]; // tomamos primer elemento como pivote
        int i = izq; // i realiza la búsqueda de izquierda a derecha
        int j = der; // j realiza la búsqueda de derecha a izquierda
        int aux;
        while (i < j) { // mientras no se crucen las búsquedas
            while (A[i] <= pivote && i < j) {
                i++; // busca elemento mayor que pivote
            }
            while (A[j] > pivote) {
                j--; // busca elemento menor que pivote
            }
            if (i < j) { // si no se han cruzado
// los intercambia
                aux = A[i];
                A[i] = A[j];
                A[j] = aux;
                i++;
                j--;
            }
        }
        if (izq != j) {
            A[izq] = A[j]; // se coloca el pivote en su lugar de forma que tendremos
            A[j] = pivote; // los menores a su izquierda y los mayores a su derecha
        }
        if (izq < j - 1) {
            quicksort(A, izq, j - 1); // ordenamos subarray izquierdo
        }
        if (j + 1 < der) {
            quicksort(A, j + 1, der); // ordenamos subarray derecho
        }
    }    


    public static void quicksort_Middle(int A[], int izq, int der) {
        int i, j, central;
        int pivote;
        central = (izq + der) / 2;
        pivote = A[central];
        i = izq;
        j = der;
        do {
            while (A[i] < pivote) {
                i++;
            }
            while (A[j] > pivote) {
                j--;
            }
            if (i <= j) {
                int aux = A[j];
                A[j] = A[i];
                A[i] = aux;
                i++;
                j--;
            }
        } while (i <= j);
        if (izq < j) {
            quicksort_Middle(A, izq, j); // ordenamos subarray izquierdo
        }
        if (i < der) {
            quicksort_Middle(A, i, der); // ordenamos subarray derecho
        }
    }


    public static void quicksort_Last(int A[], int izq, int der) {
        int pivote = A[der]; // tomamos último elemento como pivote
        int i = izq; // i realiza la búsqueda de izquierda a derecha
        int j = der; // j realiza la búsqueda de derecha a izquierda
        int aux;
        while (i < j) { // mientras no se crucen las búsquedas
            while (A[i] < pivote) {
                i++; // busca elemento mayor que pivote
            }
            while (A[j] >= pivote && i < j) {
                j--; // busca elemento min que pivote
            }
            if (i < j) { // si no se han cruzado
                aux = A[i]; // los intercambia
                A[i] = A[j];
                A[j] = aux;
                i++;
                j--;
            }
        }
        if (der != i) {
            A[der] = A[i]; // se coloca el pivote en su lugar de forma que tendremos
            A[i] = pivote; // los menores a su izquierda y los mayores a su derecha
        }
        if (izq < i - 1) {
            quicksort_Last(A, izq, i - 1); // ordenamos subarray izquierdo
        }
        if (i + 1 < der) {
            quicksort_Last(A, i + 1, der); // ordenamos subarray derecho
        }
    }    

    public static void mergesort(int A[], int izq, int der) {
        if (izq < der) {
            int m = (izq + der) / 2;
            mergesort(A, izq, m);
            mergesort(A, m + 1, der);
            merge(A, izq, m, der);
        }
    }

    public static void merge(int A[], int izq, int m, int der) {
        int i, j, k;
        int[] B = new int[A.length]; //array auxiliar
        for (i = izq; i <= der; i++) //copia ambas mitades en el array auxiliar
        {
            B[i] = A[i];
        }
        i = izq;
        j = m + 1;
        k = izq;
        while (i <= m && j <= der) //copia el siguiente elemento más grande
        {
            if (B[i] <= B[j]) {
                A[k++] = B[i++];
            } else {
                A[k++] = B[j++];
            }
        }
        while (i <= m) //copia los elementos que quedan de la
        {
            A[k++] = B[i++]; //primera mitad (si los hay)
        }
    }

    
}