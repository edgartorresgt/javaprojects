package proyectocalculadora;

import java.util.Stack;
import java.util.StringTokenizer;

public class ConvertirInfijoPrefijo {

    public static boolean esOperador(String operador) {
        return !(operador.equals("+") || operador.equals("-") || operador.equals("/") || operador.equals("*") || operador.equals("(") || operador.equals(")"));
    }

    public static boolean esNumero(String numero) {
        try {
            Integer.parseInt(numero.trim());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static String combinarOperaciones(Stack<String> OrdenOperador, Stack<String> OrdenOperando, boolean mostrarResultado) {
        String operator = OrdenOperador.pop();
        String rightOperand = OrdenOperando.pop();
        String leftOperand = OrdenOperando.pop();
        if (mostrarResultado && esNumero(rightOperand) && esNumero(leftOperand)) {
            int left = Integer.parseInt(leftOperand);
            int right = Integer.parseInt(rightOperand);
            int result = 0;
            if (operator.equals("+")) {
                result = left + right;
            } else if (operator.equals("-")) {
                result = left - right;
            } else if (operator.equals("*")) {
                result = left * right;
            } else if (operator.equals("/")) {
                result = left / right;
            }
            return "" + result;

        }
        String operand = "(" + operator + " " + leftOperand + " " + rightOperand + ")";
        return operand;
    }

    public static int orden(String s) {
        if (s.equals("+") || s.equals("-")) {
            return 1;
        } else if (s.equals("/") || s.equals("*")) {
            return 2;
        } else {
            return 0;
        }
    }

    public static String convertirInfijoAPrefijo(String OperacionInfija, boolean MostrarResultado) {
        Stack<String> operandStack = new Stack<>();
        Stack<String> operatorStack = new Stack<>();

        StringTokenizer tokenizer = new StringTokenizer(OperacionInfija);
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (esOperador(token)) {
                operandStack.push(token);
            } else if (token.equals("(") || operatorStack.isEmpty()
                    || orden(token) > orden(operatorStack.peek())) {
                operatorStack.push(token);
            } else if (token.equals(")")) {
                while (!operatorStack.peek().equals("(")) {
                    operandStack.push(combinarOperaciones(operatorStack, operandStack, MostrarResultado));
                }
                operatorStack.pop();
            } else if (orden(token) <= orden(operatorStack.peek())) {
                while (!operatorStack.isEmpty() && orden(token) <= orden(operatorStack.peek())) {
                    operandStack.push(combinarOperaciones(operatorStack, operandStack, MostrarResultado));
                }
                operatorStack.push(token);
            }
        }
        while (!operatorStack.isEmpty()) {
            operandStack.push(combinarOperaciones(operatorStack, operandStack, MostrarResultado));
        }
        return (operandStack.peek());
    }
}