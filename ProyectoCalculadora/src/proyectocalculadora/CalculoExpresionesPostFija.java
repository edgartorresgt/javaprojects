/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocalculadora;

import java.util.ArrayList;

/**
 *
 * @author Eduardo
 */
public class CalculoExpresionesPostFija {

    private int sacaTope, sacaSig, resultado;
    private ArrayList<String> operadores;
    private final Pila pilaPost;

    public CalculoExpresionesPostFija(String expresionPost[]) {
        pilaPost = new Pila(expresionPost.length);
    }

    public Double Postfija(String[] expresionPost) {//evalua una expresion dada en forma Postfija
        operadores = new ArrayList<>();//guardamos los operadores en una lista
        operadores.add("+");
        operadores.add("-");
        operadores.add("*");
        operadores.add("/");
        operadores.add("^");

        for (String car : expresionPost) {//recorremos la expresion con una variable tipo String
            if (operadores.contains(car)) {//si encontramos un operador de los listados antes entonces 
                sacaTope = pilaPost.quitar();//saca el tope
                //System.out.println("sacaTope: " + sacaTope);
                sacaSig = pilaPost.quitar();//luego saca el siguiente del tope
                //System.out.println("sacaSig: " + sacaSig);
                //System.out.println("car: " + car);
                operacionPost(sacaTope, sacaSig, car);//y realiza las operaciones que corresponden
            } else {//si encuentra un operando, lo agrega directamente a la pila
                pilaPost.poner(Integer.parseInt(car));
                //System.out.println("entra: " + car);
            }
            //pilaPost.mostrar();
        }
       return  pilaPost.mostrarResultado();//mostramos el elemento que queda en la pila
    }

    public int operacionPost(int sacaTope, int sacaSig, String operador) { //calcula la expresion en Postfija
        switch (operador) {
            case "^":
                resultado = (int) Math.pow(sacaSig, sacaTope);
                pilaPost.poner(resultado);
                break;
            case "/":
                resultado = sacaSig / sacaTope;
                pilaPost.poner(resultado);
                break;
            case "+":
                resultado = (sacaSig + sacaTope);
                pilaPost.poner(resultado);
                break;
            case "-":
                resultado = sacaSig - sacaTope;
                pilaPost.poner(resultado);
                break;
            case "*":
                resultado = sacaSig * sacaTope;
                pilaPost.poner(resultado);
                break;
            default:
                break;
        }
        return resultado;
    }
}