package Common;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

/**
 *
 * @author Edgar Torres
 */
public class mongoDBClient {

    public MongoClient getMongoClient() throws UnknownHostException {
        String mongoHost = "localhost";
        String mongoPort = "27017";
        String mongoUser = "myUserAdmin";
        String mongoPassword = "abc123";
        String mongoDBName = "MongoDBTestData";
        int port = Integer.decode(mongoPort);

        MongoCredential credential = MongoCredential.createCredential(mongoUser,
                mongoDBName,
                mongoPassword.toCharArray());

        MongoClient mongoClient = new MongoClient(new ServerAddress(mongoHost, port), Arrays.asList(credential));

        return mongoClient;
    }
}
