/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author Edgar Torres
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
public class Beer {
   private String _id;
   private String Name;
   private String Description;

	// Must have no-argument constructor
	public Beer() {

	}

	public Beer(String pId, String pName, String pDescription) {
                this._id = pId;
		this.Name = pName;
		this.Description = pDescription;
 	}

   @JsonProperty("_id")
   public String get_id() {
       return _id;
   }
   public void set_id(String id) {
       this._id = id;
   }
   
   @JsonProperty("Name")
   public String getName() {
       return Name;
   }
   
   public void setName(String name) {
       this.Name = name;
   }
   
   @JsonProperty("Description")
   public String getDescription() {
       return Description;
   }
   public void setDescription(String description) {
       this.Description = description;
   }
}
