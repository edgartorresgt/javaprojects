package Controller;

/**
 *
 * @author Edgar Torres
 */
import Common.mongoDBClient;
import Model.Beer;
import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.json.JSONArray;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.bson.types.ObjectId;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

@Path("/beer")
public class BeersController {

    private final mongoDBClient mongoClient = new mongoDBClient();
    private final String mongoDataBase = "MongoDBTestData";

    //Obtiene todo lo que tenga la coleccion Beers
    @GET()
    @Path("/beers")
    @Produces("application/json")
    public Response getAllBeers() throws UnknownHostException {

        MongoDatabase db = mongoClient.getMongoClient().getDatabase(mongoDataBase);
        MongoCollection beerCollection = db.getCollection("Beer");
        MongoCursor<Document> document = beerCollection.find().iterator();
        List<Beer> beers = new ArrayList<Beer>();
        try {
            while (document.hasNext()) {
                Document dbObject = document.next();
                Beer newBeer = new Beer(dbObject.get("_id").toString(), dbObject.get("Name").toString(), dbObject.get("Description").toString());
                beers.add(newBeer);
            }
        } finally {
            document.close();
        }

        String json = new Gson().toJson(beers);
        JSONArray objResultado = new JSONArray(json.trim());
        String jsonConFormato = objResultado.toString(4);

        return Response.ok(
                jsonConFormato,
                MediaType.APPLICATION_JSON).build();
    }

    //Obtiene un documento de lo que tenga la coleccion Beer
    @GET()
    @Path("/onebeer/{_id}")
    @Produces("application/json")
    public Response getOneBeers(@PathParam("_id") String _id) throws UnknownHostException {

        MongoDatabase db = mongoClient.getMongoClient().getDatabase(mongoDataBase);
        MongoCollection beerCollection = db.getCollection("Beer");
        MongoCursor<Document> document = beerCollection.find(Filters.eq("_id", new ObjectId(_id))).iterator();

        List<Beer> beers = new ArrayList<Beer>();
        try {
            while (document.hasNext()) {
                Document dbObject = document.next();
                Beer newBeer = new Beer(dbObject.get("_id").toString(), dbObject.get("Name").toString(), dbObject.get("Description").toString());
                beers.add(newBeer);
            }
        } finally {
            document.close();
        }

        String json = new Gson().toJson(beers);
        JSONArray objResultado = new JSONArray(json.trim());
        String jsonConFormato = objResultado.toString(4);

        return Response.ok(
                jsonConFormato,
                MediaType.APPLICATION_JSON).build();
    }

    //Agrega un nuevo registro a la coleccion del documento
    @Path("/post")
    @POST()
    @Consumes("application/json")
    public Response addOneBeer(Beer beer) throws UnknownHostException, IOException {

        MongoDatabase db = mongoClient.getMongoClient().getDatabase(mongoDataBase);
        MongoCollection beerCollection = db.getCollection("Beer");

        Document doc = new Document("Name", beer.getName())
                .append("Description", beer.getDescription());

        beerCollection.insertOne(doc);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonConFormato = ow.writeValueAsString(beer);

        return Response.ok(
                jsonConFormato,
                MediaType.APPLICATION_JSON).build();
    }

    //Actualiza un registro existente a la coleccion del documento
    @Path("/put")
    @PUT()
    @Consumes("application/json")
    public Response updateOneBeer(Beer beer) throws UnknownHostException, IOException {

        MongoDatabase db = mongoClient.getMongoClient().getDatabase(mongoDataBase);
        MongoCollection beerCollection = db.getCollection("Beer");

        beerCollection.updateOne(
                eq("_id", new ObjectId(beer.get_id())),
                combine(set("Name", beer.getName()), set("Description", beer.getDescription())));

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonConFormato = ow.writeValueAsString(beer);

        return Response.ok(
                jsonConFormato,
                MediaType.APPLICATION_JSON).build();
    }

    //Obtiene un documento de lo que tenga la coleccion Beer
    @DELETE()
    @Path("/deletebeer/{_id}")
    @Produces("application/json")
    public Response deleteOneBeers(@PathParam("_id") String _id) throws UnknownHostException {

        MongoDatabase db = mongoClient.getMongoClient().getDatabase(mongoDataBase);
        MongoCollection beerCollection = db.getCollection("Beer");
        beerCollection.deleteOne(eq("_id", new ObjectId(_id)));
        return Response.ok().build();
    }

}
