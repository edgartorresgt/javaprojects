/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Persona;
import com.dw.repository.PersonaRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class PersonaController {

    @Autowired
    PersonaRepository personaRepository;

    @RequestMapping(
            value = "personas",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Persona> getProductos() {
        List<Persona> result = (List<Persona>) personaRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "persona",
            method = RequestMethod.GET,
            produces = "application/json")
    public Persona getProduct(@RequestParam("id") Integer id) {
        Persona result = personaRepository.findOne(id);
        return result;
    }

    @RequestMapping(
            value = "persona/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<Persona> getProducts(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<Persona> result = personaRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "agregarProducto",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Persona postProduct(@RequestBody Persona persona) throws IOException {
        Persona result = personaRepository.save(persona);
        return result;
    }

    @RequestMapping(value = "ActualizarProduct",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Persona putProduct(@RequestBody Persona persona) throws IOException {
        Persona result = personaRepository.save(persona);
        return result;
    }

}