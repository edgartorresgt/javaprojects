package PilasYColas;

//Autor:Rey Salcedo Padilla
public class Main{
 public static void main(String []args){
  
  System.out.println("------Pila------");
  Pila UtilizarPila = new Pila();
  UtilizarPila.apilar("A");
  UtilizarPila.apilar("B");
  UtilizarPila.apilar("C");
  UtilizarPila.apilar("D");
  UtilizarPila.apilar("Edgar");

  System.out.println("Esta vacia la pila?:" + UtilizarPila.vacia());
  System.out.println("Tamaño de la pila:" + UtilizarPila.size());
  
  System.out.println(UtilizarPila.cima());
  UtilizarPila.desapilar();
  System.out.println(UtilizarPila.cima());
  UtilizarPila.desapilar();
  System.out.println(UtilizarPila.cima());
  UtilizarPila.desapilar();
  System.out.println(UtilizarPila.cima());
  UtilizarPila.desapilar();
  
 System.out.println(UtilizarPila.cima());
  UtilizarPila.desapilar();
  
  System.out.println("Esta vacia la pila?:" + UtilizarPila.vacia());
    
  System.out.println("------Cola------");
  Cola cola = new Cola();
  cola.encolar("A");
  cola.encolar("B");
  cola.encolar("C");
  cola.encolar("D");
  cola.encolar("Edgar");
  
  System.out.println("Esta vacia la cola?:" + cola.vacia());
  System.out.println("Tamaño de la cola:" + cola.size());
  
  System.out.println(cola.frente());
  cola.desencolar();
  System.out.println(cola.frente());
  cola.desencolar();
  System.out.println(cola.frente());
  cola.desencolar();
  System.out.println(cola.frente());
  cola.desencolar();
  System.out.println(cola.frente());
  cola.desencolar();
  
  System.out.println("Esta vacia la cola?:" + cola.vacia());
 }
}
