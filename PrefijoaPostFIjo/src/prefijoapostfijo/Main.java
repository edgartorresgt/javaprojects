package prefijoapostfijo;

public class Main {

    public static void main(String[] args) {

        //Entrada de datos
        String test = "(6+4)*3/2+(10-2)";
        System.out.println("Expresion Infija: " + test);
        
        //Expresion PostFija
        ConvertirInfijoPostFijo post = new ConvertirInfijoPostFijo();
        System.out.println("Expresion PostFija: " + post.convertirInfijoAPostFijo(test).replace(" ", ""));

        //Expresion Prefija
        StringBuilder resultadoExpresionPrefija = new StringBuilder();
        char CaracterActual, CaracterAnterior;
        for (int i = 0; i < test.length(); i++) {

            if (i > 0) {
                CaracterActual = test.charAt(i);
                CaracterAnterior = test.charAt(i - 1);
                if (Character.isDigit(CaracterActual) && !Character.isDigit(CaracterAnterior)) {
                    resultadoExpresionPrefija.append(" ");
                }
                if (!Character.isDigit(CaracterActual) && Character.isDigit(CaracterAnterior)) {
                    resultadoExpresionPrefija.append(" ");
                }
                if (!Character.isDigit(CaracterActual) && !Character.isDigit(CaracterAnterior)) {
                    resultadoExpresionPrefija.append(" ");
                }
            }
            resultadoExpresionPrefija.append(test.charAt(i));
        }
        ConvertirInfijoPrefijo pre = new ConvertirInfijoPrefijo();
        System.out.println("Expresion Prefija: " + pre.convertirInfijoAPrefijo(resultadoExpresionPrefija.toString(), false).replace(" ", ""));
        System.out.println("Resultado Expresion Prefija: " + pre.convertirInfijoAPrefijo(resultadoExpresionPrefija.toString(), true));

    }

}
