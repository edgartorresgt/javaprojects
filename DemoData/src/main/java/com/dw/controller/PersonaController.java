/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Persona;
import com.dw.repository.PersonaRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class PersonaController {

    @Autowired
    PersonaRepository marcaRepository;

    @RequestMapping(
            value = "personas",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Persona> gePersonas() {
        List<Persona> result = (List<Persona>) marcaRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "persona",
            method = RequestMethod.GET,
            produces = "application/json")
    public Persona getPersona(@RequestParam("id") Integer id_marca) {
        Persona result = marcaRepository.findOne(id_marca);
        return result;
    }

    @RequestMapping(
            value = "personas/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<Persona> getPagePersona(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<Persona> result = marcaRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "addpersona",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Persona postPersona(@RequestBody Persona marca) throws IOException {
        Persona result = marcaRepository.save(marca);
        return result;
    }

    @RequestMapping(value = "updatepersona",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Persona putPersona(@RequestBody Persona marca) throws IOException {
        Persona result = marcaRepository.save(marca);
        return result;
    }

}
