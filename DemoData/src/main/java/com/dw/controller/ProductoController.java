/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Producto;
import com.dw.repository.ProductoRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class ProductoController {

    @Autowired
    ProductoRepository marcaRepository;

    @RequestMapping(
            value = "productos",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Producto> getproductos() {
        List<Producto> result = (List<Producto>) marcaRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "producto",
            method = RequestMethod.GET,
            produces = "application/json")
    public Producto getproducto(@RequestParam("id") Integer id_marca) {
        Producto result = marcaRepository.findOne(id_marca);
        return result;
    }

    @RequestMapping(
            value = "productos/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<Producto> getPageProducto(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<Producto> result = marcaRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "addProducto",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Producto postProducto(@RequestBody Producto marca) throws IOException {
        Producto result = marcaRepository.save(marca);
        return result;
    }

    @RequestMapping(value = "updateProducto",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Producto putProducto(@RequestBody Producto marca) throws IOException {
        Producto result = marcaRepository.save(marca);
        return result;
    }

}
