/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Marca;
import com.dw.repository.MarcaRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class MarcaController {

    @Autowired
    MarcaRepository marcaRepository;

    @RequestMapping(
            value = "marcas",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Marca> getMarcas() {
        List<Marca> result = (List<Marca>) marcaRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "marca",
            method = RequestMethod.GET,
            produces = "application/json")
    public Marca getMarca(@RequestParam("id") Integer id_marca) {
        Marca result = marcaRepository.findOne(id_marca);
        return result;
    }

    @RequestMapping(
            value = "marcas/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<Marca> getPageMarca(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<Marca> result = marcaRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "addMarca",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Marca postMarca(@RequestBody Marca marca) throws IOException {
        Marca result = marcaRepository.save(marca);
        return result;
    }

    @RequestMapping(value = "updateMarca",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Marca putProduct(@RequestBody Marca marca) throws IOException {
        Marca result = marcaRepository.save(marca);
        return result;
    }

}
