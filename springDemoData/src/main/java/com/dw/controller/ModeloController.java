/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Modelo;
import com.dw.repository.ModeloRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class ModeloController {

    @Autowired
    ModeloRepository modeloRepository;

    @RequestMapping(
            value = "modelos",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Modelo> getProductos() {
        List<Modelo> result = (List<Modelo>) modeloRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "modelo",
            method = RequestMethod.GET,
            produces = "application/json")
    public Modelo getProduct(@RequestParam("id") Integer id_modelo) {
        Modelo result = modeloRepository.findOne(id_modelo);
        return result;
    }

    @RequestMapping(
            value = "modelos/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<Modelo> getProducts(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<Modelo> result = modeloRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "addModelo",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Modelo postProduct(@RequestBody Modelo modelo) throws IOException {
        Modelo result = modeloRepository.save(modelo);
        return result;
    }

    @RequestMapping(value = "updateModelo",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Modelo putProduct(@RequestBody Modelo modelo) throws IOException {
        Modelo result = modeloRepository.save(modelo);
        return result;
    }

}
