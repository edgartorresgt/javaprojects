/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.Product;
import com.dw.repository.ProductRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(
            value = "products",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<Product> getProductos() {
        List<Product> result = (List<Product>) productRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "product",
            method = RequestMethod.GET,
            produces = "application/json")
    public Product getProduct(@RequestParam("id") Integer id) {
        Product result = productRepository.findOne(id);
        return result;
    }

    @RequestMapping(
            value = "products/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<Product> getProducts(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<Product> result = productRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "addProduct",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Product postProduct(@RequestBody Product product) throws IOException {
        Product result = productRepository.save(product);
        return product;
    }

    @RequestMapping(value = "addProductSp",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseStatus postProductSP(@RequestBody Product product) throws IOException {
        try {
            productRepository.crear_producto(product.getName());
            return (ResponseStatus) new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return (ResponseStatus) new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "updateProduct",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Product putProduct(@RequestBody Product product) throws IOException {
        Product result = productRepository.save(product);
        return result;
    }

}
