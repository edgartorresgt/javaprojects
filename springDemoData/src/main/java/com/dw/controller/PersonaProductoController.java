/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.PersonaProducto;
import com.dw.repository.PersonaProductoRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class PersonaProductoController {
    
    @Autowired
    PersonaProductoRepository personaProductoRepository; 

    @RequestMapping(
            value = "personasProductos",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<PersonaProducto> getProductos() {
        List<PersonaProducto> result = (List<PersonaProducto>) personaProductoRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "personaProducto",
            method = RequestMethod.GET,
            produces = "application/json")
    public PersonaProducto getProduct(@RequestParam("id") Integer id) {
        PersonaProducto result = personaProductoRepository.findOne(id);
        return result;
    }

    @RequestMapping(
            value = "personaProducto/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<PersonaProducto> getProducts(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<PersonaProducto> result = personaProductoRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "addPersonaProducto",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public PersonaProducto postProduct(@RequestBody PersonaProducto marcamodelo) throws IOException {
        PersonaProducto result = personaProductoRepository.save(marcamodelo);
        return result;
    }


    @RequestMapping(value = "updateMarcaModelo",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public PersonaProducto putProduct(@RequestBody PersonaProducto marcamodelo) throws IOException {
        PersonaProducto result = personaProductoRepository.save(marcamodelo);
        return result;
    }

}
