/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.controller;

import com.dw.entity.MarcaModelo;
import com.dw.repository.MarcaModeloRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edgar Torres
 */
@RestController
@EnableJpaRepositories(basePackages = "com.dw.repository")
public class MarcaModeloController {
    
    @Autowired
    MarcaModeloRepository marcaModeloRepository; 

    @RequestMapping(
            value = "MarcasModelos",
            method = RequestMethod.GET,
            produces = "application/json")
    public List<MarcaModelo> getProductos() {
        List<MarcaModelo> result = (List<MarcaModelo>) marcaModeloRepository.findAll();
        return result;
    }

    @RequestMapping(
            value = "MarcaModelo",
            method = RequestMethod.GET,
            produces = "application/json")
    public MarcaModelo getProduct(@RequestParam("id") Integer id) {
        MarcaModelo result = marcaModeloRepository.findOne(id);
        return result;
    }

    @RequestMapping(
            value = "MarcasModelos/page",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Page<MarcaModelo> getProducts(@RequestParam("pagenumber") Integer pagenumber,
            @RequestParam("size") Integer size) {
        Page<MarcaModelo> result = marcaModeloRepository.findAll(new PageRequest(pagenumber, size));
        return result;
    }

    @RequestMapping(value = "addMarcaModelo",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public MarcaModelo postProduct(@RequestBody MarcaModelo marcamodelo) throws IOException {
        MarcaModelo result = marcaModeloRepository.save(marcamodelo);
        return result;
    }


    @RequestMapping(value = "updateMarcaModelo",
            method = RequestMethod.PUT,
            produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public MarcaModelo putProduct(@RequestBody MarcaModelo marcamodelo) throws IOException {
        MarcaModelo result = marcaModeloRepository.save(marcamodelo);
        return result;
    }

}
