/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Edgar Torres
 */
@Entity
@Table(name = "marca_modelo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarcaModelo.findAll", query = "SELECT m FROM MarcaModelo m"),
    @NamedQuery(name = "MarcaModelo.findByIdMarcaModelo", query = "SELECT m FROM MarcaModelo m WHERE m.idMarcaModelo = :idMarcaModelo")})
public class MarcaModelo implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_marca_modelo")
    
    private Integer idMarcaModelo;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_marca", referencedColumnName = "id_marca")
    private Marca idMarca;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_modelo", referencedColumnName = "id_modelo")
    private Modelo idModelo;

    public MarcaModelo() {
    }

    public MarcaModelo(Integer idMarcaModelo) {
        this.idMarcaModelo = idMarcaModelo;
    }

    public Integer getIdMarcaModelo() {
        return idMarcaModelo;
    }

    public void setIdMarcaModelo(Integer idMarcaModelo) {
        this.idMarcaModelo = idMarcaModelo;
    }

    public Marca getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Marca idMarca) {
        this.idMarca = idMarca;
    }

    public Modelo getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Modelo idModelo) {
        this.idModelo = idModelo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMarcaModelo != null ? idMarcaModelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarcaModelo)) {
            return false;
        }
        MarcaModelo other = (MarcaModelo) object;
        if ((this.idMarcaModelo == null && other.idMarcaModelo != null) || (this.idMarcaModelo != null && !this.idMarcaModelo.equals(other.idMarcaModelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dw.entity.MarcaModelo[ idMarcaModelo=" + idMarcaModelo + " ]";
    }
    
}
