/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.repository;

import com.dw.entity.Marca;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author Edgar Torres
 */
public interface MarcaRepository extends PagingAndSortingRepository<Marca, Integer>, QueryByExampleExecutor <Marca>{
    
}
