/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dw.repository;

import com.dw.entity.Product;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author Edgar Torres
 */
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>, QueryByExampleExecutor <Product> {
    @Procedure(name = "crear_producto")
    void crear_producto(@Param("name") String name);
}